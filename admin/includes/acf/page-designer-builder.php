<?php

class page_designer_builder extends acf_field {

    /**
     * The ID of this plugin.
     *
     * @since    1.0.0
     * @access   private
     * @var      string    $plugin_name    The ID of this plugin.
     */
    private $plugin_name;

    private $plugin_dir_url;

    var $settings
    , $defaults
    , $domain
    , $lang
    , $notice_arg;


    function __construct( $plugin_name, $plugin_dir, $plugin_dir_url, $version ) {

        $this->plugin_name = $plugin_name;
        $this->plugin_dir = $plugin_dir;
        $this->plugin_dir_url = $plugin_dir_url;

        // vars
        $this->name = $this->plugin_name;
        $this->label = 'Page Designer';
        $this->category = __('Basic', 'dachcom-page-designer');
        $this->domain = 'dachcom-page-designer';
        $this->notice_args = array();

        // do not delete!
        parent::__construct();

        // settings
        $this->settings = array(

            'path'      => $plugin_dir_url,
            'dir'     => $plugin_dir_url,
            'version' => $version
        );

    }

    function input_admin_enqueue_scripts() {

        $theme_columns = get_option('dpd_setting-theme_column');

        if(!file_exists( $this->plugin_dir . 'css/visualizer_'. $theme_columns .'.css' )) {

            //class => updated, error
            $this->notice_args = array(
                'class' => 'error',
                'msg' => sprintf( __('Es wurde keine CSS mit dem Namen "visualizer_%s.css" gefunden.', 'dachcom-page-designer'), $theme_columns )
            );

            add_action( 'admin_notices', array($this, 'admin_notices'));

        } else {

            wp_enqueue_style( 'extend_acf_flexible_field_visualizer_' . $theme_columns . '_css', $this->plugin_dir_url . 'css/visualizer_'. $theme_columns .'.css', array(), $this->settings['version'] );

        }

        wp_enqueue_style( 'extend_acf_flexible_field_visualizer_default_css', $this->plugin_dir_url . 'css/admin.css', array(), $this->settings['version'] );

        // Register the script
        wp_register_script( 'extend_acf_flexible_field_visualizer_js', $this->plugin_dir_url .'js/admin.js', array(), $this->settings['version'] );

        // Localize the script with new data
        $translation_array = array(
            'cleanTextVal' => __( 'Keine Vorschau verfügbar.', 'dachcom-page-designer' ),
            'editContent' => __( 'Inhalt bearbeiten', 'dachcom-page-designer' ),
            'saveClose' => __( 'Speichern & Schließen', 'dachcom-page-designer' ),
            'escNote' => __( 'Hinweis: Wenn Sie das Fenster über ESC oder das Schließen-Symbol rechts oben schließen, wird der Inhalt nicht gespeichert.', 'dachcom-page-designer' ),
            'responsive' => __( 'Responsive Adjustierung vornehmen', 'dachcom-page-designer' )
            );

        wp_localize_script( 'extend_acf_flexible_field_visualizer_js', 'translation_object', $translation_array );

        // Enqueued script with localized data.
        wp_enqueue_script( 'extend_acf_flexible_field_visualizer_js' );

    }

    function admin_notices(){

        if(!current_user_can('update_core'))
            return;

        echo '<div class="'. $this->notice_args['class'] .'">';
            echo '<p>';
                echo '<b>'. $this->label .':</b> ' . $this->notice_args['msg'];
            echo '</p>';
        echo '</div>';
    }


}