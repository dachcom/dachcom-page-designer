<?php

/**
 * The public-facing functionality of the plugin.
 *
 * @link       http://www.dachcom.com
 * @since      1.0.0
 *
 * @package    Dachcom_Page_Designer
 * @subpackage Dachcom_Page_Designer/public
 */

/**
 * The public-facing functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the admin-specific stylesheet and JavaScript.
 *
 * @package    Dachcom_Page_Designer
 * @subpackage Dachcom_Page_Designer/public
 * @author     Stefan Hagspiel <shagspiel@dachcom.ch>
 */
class Dachcom_Page_Designer_Public {

	/**
	 * The ID of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string $plugin_name
	 */
	private $plugin_name;

	/**
	 * The version of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string $version
	 */
	private $version;

    /**
     * @var Dachcom_Page_Designer_ModuleLoader
     */
    private $dpd_moduleloader;

	/**
	 * Initialize the class and set its properties.
	 *
	 * @since       1.0.0
	 * @param      string $plugin_name
	 * @param      string $version
	 */
	public function __construct( $plugin_name, $version ) {

		$this->plugin_name = $plugin_name;
		$this->version = $version;

    }

	public function initialize_module_controller() {

        $this->dpd_moduleloader = new Dachcom_Page_Designer_ModuleLoader();

	}

}
